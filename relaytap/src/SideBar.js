import React from 'react';
import NavSideBar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';

export default class SideBar extends React.Component {

    sideBarJsx(){
        return(
            <Nav.Item className="nav-sidebar-item">
                <Nav.Link>
                    <Button variant='primary'>Place Holder</Button>
                </Nav.Link>
            </Nav.Item>
        )
    }
    render() {
        return (
          <NavSideBar expand="lg" fixed="top" className="sidenav">
            <Container className="centered">
              <Nav className="centered">
                {this.sideBarJsx()}
              </Nav>
            </Container>
          </NavSideBar>
        )
        }
}