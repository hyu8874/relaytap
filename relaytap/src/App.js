import React from 'react';
import Container from 'react-bootstrap/Container';
import NavBar from './NavBar';
import SideBar from './SideBar'
import './App.css';

function App() {
  return (
      <Container className="App" fluid>
        <NavBar className="header"/>
        <SideBar className="side"/>
      </Container>
  );
}

export default App;
