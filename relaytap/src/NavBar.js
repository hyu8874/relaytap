import React from 'react';
import NavBar from 'react-bootstrap/Navbar';
import Nav from 'react-bootstrap/Nav';
import Container from 'react-bootstrap/Container';
import Button from 'react-bootstrap/Button';
import Logo from './tapqa.png';

export default class Navbar extends React.Component {

    navJsx() {
        return (
            <Nav.Item className="nav-bar-item">
                <Nav.Link>
                    <Button variant='primary'>Place Holder</Button>
                </Nav.Link>
            </Nav.Item>
        )
      }


  render() {
    return (
      <NavBar expand="lg" fixed="top" className="nav-container">
        <img src={Logo} className='logo' alt="Logo"/>
        <Container className="centered">
          <Nav className="centered">
            {this.navJsx()}
          </Nav>
        </Container>
      </NavBar>
    )
  }
}